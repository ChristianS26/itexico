package com.christianm.itexico.android_native.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.christianm.itexico.android_native.ClientsComponent;
import com.christianm.itexico.android_native.DaggerClientsComponent;
import com.christianm.itexico.android_native.client.MovieClient;
import com.christianm.itexico.android_native.model.Movie;

import io.reactivex.disposables.CompositeDisposable;

public class MoviesViewModel extends ViewModel implements MovieClient.ClientCallBack.moviesCallback {

    private MutableLiveData<Movie> movie = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    private ClientsComponent component = DaggerClientsComponent.create();

    public LiveData<Movie> getPopularMovies(int page, String language) {
        getMovies(page, language);
        return movie;
    }

    public LiveData<Movie> getTopRated(int page, String language) {
        getTopMovies(page, language);
        return movie;
    }

    public void dispose(){
        disposable.clear();
    }


    private void getMovies(int page, String language) {
        disposable.add(component.getMovieClient().getPopularMovies(this, page, language));
    }

    private void getTopMovies(int page, String language) {
        disposable.add(component.getMovieClient().getTopRatedMovies(this, page, language));
    }

    @Override
    public void onSucceed(Movie movie) {
        this.movie.setValue(movie);
    }

    @Override
    public void onError(Throwable error) {
        Log.wtf("onError", error.toString());
    }
}
