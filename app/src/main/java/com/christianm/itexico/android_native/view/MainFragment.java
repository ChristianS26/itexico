package com.christianm.itexico.android_native.view;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.christianm.itexico.R;
import com.christianm.itexico.android_native.adapters.MoviesAdapter;
import com.christianm.itexico.android_native.listeners.iMovieClickListener;
import com.christianm.itexico.android_native.model.Movie;
import com.christianm.itexico.android_native.viewmodel.MoviesViewModel;
import com.christianm.itexico.databinding.FragmentMainBinding;


public class MainFragment extends Fragment implements iMovieClickListener {
    private MoviesAdapter moviesAdapter;
    private MoviesViewModel model;
    private FragmentMainBinding fragmentMainBinding;
    private boolean isLoading = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);

        model = ViewModelProviders.of(this).get(MoviesViewModel.class);

        setTitle();

        if (isLoading)
            fragmentMainBinding.progressCircular.setVisibility(View.VISIBLE);


        observePopular();
        return fragmentMainBinding.getRoot();
    }

    private void setTitle() {
        if (null != ((AppCompatActivity) getActivity()).getSupportActionBar()) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Movies");
            setHasOptionsMenu(true);
        }
    }

    private void observePopular() {
        model.getPopularMovies(1, getString(R.string.language)).observe(this, movie -> {
            isLoading = false;
            fragmentMainBinding.progressCircular.setVisibility(View.GONE);
            fragmentMainBinding.rvMovies.setVisibility(View.VISIBLE);
            moviesAdapter = new MoviesAdapter(getContext(), movie.results, this);
            fragmentMainBinding.rvMovies.setLayoutManager(new GridLayoutManager(getContext(), 3));
            fragmentMainBinding.rvMovies.setAdapter(moviesAdapter);
        });
    }

    private void observeTopRated() {
        model.getTopRated(1, getString(R.string.language)).observe(this, movie -> {
            moviesAdapter = new MoviesAdapter(getContext(), movie.results, this);
            fragmentMainBinding.rvMovies.setLayoutManager(new GridLayoutManager(getContext(), 3));
            fragmentMainBinding.rvMovies.setAdapter(moviesAdapter);
        });
    }

    @Override
    public void onMovieClick(Movie.results movie) {
        Bundle args = new Bundle();
        args.putInt("id", movie.id);
        NavHostFragment.findNavController(this).navigate(R.id.movieDetailsFragment, args);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popular:
                observePopular();
                break;
            case R.id.top_rated:
                observeTopRated();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.dispose();
    }
}
