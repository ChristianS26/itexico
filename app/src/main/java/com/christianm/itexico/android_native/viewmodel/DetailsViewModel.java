package com.christianm.itexico.android_native.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.christianm.itexico.android_native.ClientsComponent;
import com.christianm.itexico.android_native.DaggerClientsComponent;
import com.christianm.itexico.android_native.client.MovieClient;
import com.christianm.itexico.android_native.model.Details;

import io.reactivex.disposables.CompositeDisposable;

public class DetailsViewModel extends ViewModel implements MovieClient.ClientCallBack.detailsCallback {

    private MutableLiveData<Details> details;

    private CompositeDisposable disposable = new CompositeDisposable();
    private ClientsComponent component = DaggerClientsComponent.create();

    public LiveData<Details> getMovieDetails(int movie_id, String language) {
        if (details == null) {
            details = new MutableLiveData<>();
            getDetails(movie_id, language);
        }
        return details;
    }


    private void getDetails(int movie_id, String language) {
        disposable.add(component.getMovieClient().getDetails(this, movie_id, language));
    }

    public void dispose() {
        disposable.clear();
    }

    @Override
    public void onSucceed(Details details) {
        this.details.setValue(details);
    }

    @Override
    public void onError(Throwable error) {
        Log.wtf("onError", error.toString());
    }
}
