package com.christianm.itexico.android_native.listeners;


import com.christianm.itexico.android_native.model.Movie;

public interface iMovieClickListener {

    void onMovieClick(Movie.results movie);
}
