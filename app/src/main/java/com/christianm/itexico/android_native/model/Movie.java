package com.christianm.itexico.android_native.model;

import java.util.List;

public class Movie {

    int page;
    int total_results;
    int total_pages;
    public List<results> results;

    @Override
    public String toString() {
        String value = "";

        if (null != results) {

            for (Movie.results result : results) {
                value += "\n name: " + result.title;
            }
        }

        return value;
    }

    public Movie(int page, int total_results, int total_pages, List<results> results) {
        this.page = page;
        this.total_results = total_results;
        this.total_pages = total_pages;
        this.results = results;
    }

    public class results {

        public String poster_path;
        public int id;
        public String title;
        boolean adult;
        String overview;
        String release_date;
        int[] genre_ids;
        String original_title;
        String original_lenguage;
        String backdrop_path;
        Double popularity;
        int vote_count;
        boolean video;
        Double vote_average;


    }
}
