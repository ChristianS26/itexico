package com.christianm.itexico.android_native;


import com.christianm.itexico.android_native.client.MovieClient;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component
public interface ClientsComponent {

    MovieClient getMovieClient();
}
