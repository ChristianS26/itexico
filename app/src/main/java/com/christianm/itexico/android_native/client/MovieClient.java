package com.christianm.itexico.android_native.client;

import com.christianm.itexico.BuildConfig;
import com.christianm.itexico.android_native.model.Details;
import com.christianm.itexico.android_native.model.Movie;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class MovieClient {

    private iMovieClient client;

    @Inject
    public MovieClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        client = retrofit.create(iMovieClient.class);
    }


    public Disposable getPopularMovies(ClientCallBack.moviesCallback callBack, int page, String language) {
        return client.getPopularMovies(page, language, BuildConfig.PUBLIC_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        callBack::onSucceed,
                        callBack::onError
                );
    }

    public Disposable getTopRatedMovies(ClientCallBack.moviesCallback callBack, int page, String language) {
        return client.getTopRated(page, language, BuildConfig.PUBLIC_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        callBack::onSucceed,
                        callBack::onError
                );
    }

    public Disposable getDetails(ClientCallBack.detailsCallback callBack, int movie_id, String language) {
        return client.getDetails(movie_id, language, BuildConfig.PUBLIC_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        callBack::onSucceed,
                        callBack::onError
                );
    }


    public interface ClientCallBack {
        interface moviesCallback {
            void onSucceed(Movie movie);

            void onError(Throwable error);
        }

        interface detailsCallback {
            void onSucceed(Details details);

            void onError(Throwable error);
        }

    }
}
