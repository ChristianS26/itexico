package com.christianm.itexico.android_native.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.christianm.itexico.BuildConfig;
import com.christianm.itexico.R;
import com.christianm.itexico.android_native.listeners.iMovieClickListener;
import com.christianm.itexico.android_native.model.Movie;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    private List<Movie.results> movieList;
    private iMovieClickListener listener;
    private Context context;

    public MoviesAdapter(Context context, List<Movie.results> movieList, iMovieClickListener listener) {
        this.context = context;
        this.movieList = movieList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MoviesViewHolder(LayoutInflater.from(context).inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, int position) {

        Glide.with(context)
                .load(BuildConfig.BASEURL_IMG + movieList.get(position).poster_path)
                .into(holder.imagePoster);

        holder.imagePoster.setOnClickListener(v -> listener.onMovieClick(movieList.get(position)));

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    class MoviesViewHolder extends RecyclerView.ViewHolder {
        ImageView imagePoster;

        private MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePoster = itemView.findViewById(R.id.movie_poster);
        }
    }
}
