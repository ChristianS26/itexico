package com.christianm.itexico.android_native.view;

import android.os.Bundle;



import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.christianm.itexico.R;
import com.christianm.itexico.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(activityMainBinding.toolbar);
    }
}
