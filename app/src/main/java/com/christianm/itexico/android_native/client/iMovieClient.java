package com.christianm.itexico.android_native.client;



import com.christianm.itexico.android_native.model.Details;
import com.christianm.itexico.android_native.model.Movie;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface iMovieClient {


    @GET("movie/popular?")
    Observable<Movie> getPopularMovies(@Query("page") int page, @Query("language") String language, @Query("api_key") String apiKey);

    @GET("movie/top_rated?")
    Observable<Movie> getTopRated(@Query("page") int page, @Query("language") String language, @Query("api_key") String apiKey);

    @GET("movie/{movie_id}?")
    Observable<Details> getDetails(@Path("movie_id") int id, @Query("language") String language, @Query("api_key") String apiKey);

}
