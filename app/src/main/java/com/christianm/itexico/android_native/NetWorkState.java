package com.christianm.itexico.android_native;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class NetWorkState {
    private Context appContext;

    public boolean isConnected() {
        return isConnected;
    }

    private boolean isConnected = false;

    public NetWorkState(Context context) {
        this.appContext = context;
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onReceive(Context context, Intent intent) {
                NetworkInfo wifi = null;
                NetworkInfo mobileData = null;
                ConnectivityManager connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager != null) {
                    wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    mobileData = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                }


                if (wifi != null && (wifi.isAvailable() || mobileData.isAvailable()) && isConnected(appContext)) {
                    isConnected = true;
                }

            }
        };
        context.registerReceiver(broadcastReceiver,intentFilter);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isConnected (Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetowork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetowork != null  && activeNetowork.isConnectedOrConnecting();
    }
}
